# **Marcin Pałka**
###### _Curriculum Vitae_
&nbsp;
![A pretty tiger](https://upload.wikimedia.org/wikipedia/commons/5/56/Tiger.50.jpg)


### **Dane osobowe**
###### **Data urodzenia:** _30.08.1996_ 
###### **Miejsce zamieszkania:** _Lublin_
###### **Adres:** _ul. XYZ 00, 00-000_
###### **Telefon:** _123 456 789_
###### **Adres e-mail:** [marcin.palka191@wp.pl](marcin.palka191@wp.pl)
&nbsp;
### **Wykształcenie**
###### _2012 - 2015_ - **III Liceum Ogólnokształcące im. Unii Lubelskiej w Lublinie** - **Profil matematyczno-geograficzny**
###### _2015 - 2018_ - **Uniwersytet Marii Curie-Skłodowskiej** - **Geoinformatyka, licencjackie**
###### _2018 - 2020_ - **Uniwersytet Marii Curie-Skłodowskiej** - **Geoinformatyka, magisterskie**
&nbsp;
### **Doświadczenie zawodowe**
###### _2016_ - **ZZZZZZ**
###### _2017_ - **AAAAAA**
###### _2018_ - **BBBBBB**
###### _2019_ - **CCCCCC**
&nbsp;
### **Języki**
###### _angielski_ - **zaawansowany**
###### _niemiecki_ - **podstawowy**
###### _rosyjski_ - **podstawowy**
&nbsp;
### **Zainteresowania**
###### _muzyka_
###### _sport_
###### _historia_
###### _sztuka_
###### _nowe technologie_





